<?php
$page_name = 'data_list';

require __DIR__ . '/__connect_db.php';

$per_page = 5;
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
/*
$t_sql = "SELECT 1 FROM `address_book`";
$t_result = $mysqli->query($t_sql);
$total_rows = $t_result->num_rows;
*/


$t_sql = "SELECT count(1) FROM `address_book`";
$t_result = $mysqli->query($t_sql);
$total_rows = $t_result->fetch_row()[0];

$total_pages = ceil($total_rows/$per_page);
$page = $page>$total_pages ? $total_pages : $page;


//$sql = sprintf("SELECT * FROM `address_book` LIMIT %s, %s", ($page-1)*$per_page, $per_page);
$sql = sprintf("SELECT * FROM `address_book` ORDER BY `sid` DESC LIMIT %s, %s", ($page-1)*$per_page, $per_page);

$result = $mysqli->query($sql);

//$row = $result->fetch_assoc();
//echo $total_rows. '<br>';
//echo $total_pages. '<br>';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">

    <script src="lib/jquery-3.1.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <style>
        .glyphicon-remove-sign {
            font-size: 24px;
            color: red;
        }
    </style>
</head>
<body>
<div class="container">

    <?php include __DIR__. '/__navbar.php'; ?>

    <nav aria-label="...">
        <ul class="pager">
            <?php if($page==1): ?>
                <li class="disabled"><a>First</a></li>
                <li class="disabled"><a>Previous</a></li>
            <?php else: ?>
                <li><a href="?page=1">First</a></li>
                <li><a href="?page=<?= $page-1 ?>">Previous</a></li>
            <?php endif; ?>

            <li><?= $page. ' / '. $total_pages ?></li>

            <?php if($page==$total_pages): ?>
                <li class="disabled"><a>Next</a></li>
                <li class="disabled"><a>Last</a></li>
            <?php else: ?>
                <li><a href="?page=<?= $page+1 ?>">Next</a></li>
                <li><a href="?page=<?= $total_pages ?>">Last</a></li>
            <?php endif; ?>
        </ul>
    </nav>

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>刪除</th>
            <th>sid</th>
            <th>name</th>
            <th>phone</th>
            <th>email</th>
            <th>birthday</th>
            <th>address</th>
            <th>編輯</th>
        </tr>
        </thead>
        <tbody>
        <?php while ($row = $result->fetch_assoc()): ?>
            <tr>
                <td>
                    <a href="data_delete.php?sid=<?= $row['sid'] ?>">
                        <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
                    </a>
                </td>
                <td><?= $row['sid'] ?></td>
                <td><?= $row['name'] ?></td>
                <td><?= $row['phone'] ?></td>
                <td><?= $row['email'] ?></td>
                <td><?= $row['birthday'] ?></td>
                <!-- strip_tags()  -->
                <td><?= htmlentities($row['address']) ?></td>
                <td>
                    <a href="data_edit.php?sid=<?= $row['sid'] ?>">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>
                </td>

            </tr>
        <?php endwhile; ?>
        </tbody>
    </table>


</div>


</body>
</html>