<?php
$page_name = 'data_edit';

require __DIR__ . '/__connect_db.php';

/*
 //第一種用法 $mysqli->query()
$sid = isset($_GET['sid']) ? $_GET['sid'] : '';

$sql = sprintf("SELECT * FROM `address_book` WHERE `sid`= '%s'",
    $mysqli->escape_string($sid)
    );
$result = $mysqli->query($sql);
$row = $result->fetch_assoc();

if(empty($row)){
    //$r_error_msg = "無資料";
    header("Location: data_list.php");
}
*/

/*
 //第二種用法 $mysqli->query()
$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;

$sql = "SELECT * FROM `address_book` WHERE `sid`= $sid";
$result = $mysqli->query($sql);
$row = $result->fetch_assoc();

if(empty($row)){
    //$r_error_msg = "無資料";
    header("Location: data_list.php");
}
*/

//第三種用法
$sid = isset($_GET['sid']) ? $_GET['sid'] : '';

$stmt = $mysqli->prepare("SELECT * FROM `address_book` WHERE `sid`=?");

$stmt->bind_param('i', $sid);
$stmt->execute();

$stmt->bind_result($c_sid, $c_name, $c_phone, $c_email, $c_birthday, $c_address);
$stmt->fetch();

$stmt->close();



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">

    <script src="lib/jquery-3.1.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
</head>
<body>
<div class="container">

    <?php include __DIR__ . '/__navbar.php'; ?>

    <?php if(isset($r_info) and $r_info===true ): ?>
        <div class="alert alert-success" role="alert">修改完成</div>
    <?php endif; ?>


    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-danger ">
            <div class="panel-heading"><h3 class="panel-title">修改資料</h3></div>
            <div class="panel-body">

                <form name="form1" method="post">
                    <div class="form-group">
                        <label for="">姓名:</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="姓名" required
                        value="<?= $c_name ?>">
                    </div>

                    <div class="form-group">
                        <label for="">電話:</label>
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="電話"
                               value="<?= $c_phone ?>">
                    </div>

                    <div class="form-group">
                        <label for="">電郵:</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="email"
                               value="<?= $c_email ?>">
                    </div>

                    <div class="form-group">
                        <label for="">生日:</label>
                        <input type="date" class="form-control" name="birthday" id="birthday" placeholder="YYY-MM-DD"
                               value="<?= $c_birthday ?>">
                    </div>

                    <div class="form-group">
                        <label for="address">地址:</label>
                        <textarea  class="form-control" name="address" id="address" cols="30" rows="10"><?= $c_address ?></textarea>
                    </div>
                    <button type="submit" class="btn btn-default">送出</button>
<?php /*
                    <?php if(isset($r_info) and $r_info===true ): ?>
                        <script>
                            $('input, textarea').prop('disabled', 'disabled');
                        </script>

                    <?php else: ?>
                        <button type="submit" class="btn btn-default">送出</button>
                    <?php endif; ?>
*/ ?>
                </form>

            </div>
        </div>
    </div>



</div>


</body>
</html>