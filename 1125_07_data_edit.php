<?php
//$page_name = 'data_insert';

require __DIR__ . '/__connect_db.php';





?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">

    <script src="lib/jquery-3.1.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
</head>
<body>
<div class="container">

    <?php include __DIR__ . '/__navbar.php'; ?>

    <?php if(isset($r_info) and $r_info===true ): ?>
        <div class="alert alert-success" role="alert">修改完成</div>
    <?php endif; ?>


    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-danger ">
            <div class="panel-heading"><h3 class="panel-title">修改資料</h3></div>
            <div class="panel-body">

                <form name="form1" method="post">
                    <div class="form-group">
                        <label for="">姓名:</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="姓名" required
                        value="<?= isset($_POST['name']) ? $_POST['name'] : '' ?>">
                    </div>

                    <div class="form-group">
                        <label for="">電話:</label>
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="電話"
                               value="<?= isset($_POST['phone']) ? $_POST['phone'] : '' ?>">
                    </div>

                    <div class="form-group">
                        <label for="">電郵:</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="email"
                               value="<?= isset($_POST['email']) ? $_POST['email'] : '' ?>">
                    </div>

                    <div class="form-group">
                        <label for="">生日:</label>
                        <input type="date" class="form-control" name="birthday" id="birthday" placeholder="YYY-MM-DD"
                               value="<?= isset($_POST['birthday']) ? $_POST['birthday'] : '' ?>">
                    </div>

                    <div class="form-group">
                        <label for="address">地址:</label>
                        <textarea  class="form-control" name="address" id="address" cols="30" rows="10"><?= isset($_POST['address']) ? $_POST['address'] : '' ?></textarea>
                    </div>

                    <?php if(isset($r_info) and $r_info===true ): ?>
                        <script>
                            $('input, textarea').prop('disabled', 'disabled');
                        </script>

                    <?php else: ?>
                        <button type="submit" class="btn btn-default">送出</button>
                    <?php endif; ?>

                </form>

            </div>
        </div>
    </div>



</div>


</body>
</html>