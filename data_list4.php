<?php
//include __DIR__. '/__connect_db.php';
//include_once __DIR__. '/__connect_db.php';
//require_once __DIR__. '/__connect_db.php';
require __DIR__ . '/__connect_db.php';

$sql = "SELECT * FROM `address_book`";

$result = $mysqli->query($sql);

//$row = $result->fetch_assoc();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">

    <script src="lib/jquery-3.1.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Brand</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                    <li><a href="#">Link</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Link</a></li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>sid</th>
            <th>name</th>
            <th>phone</th>
            <th>email</th>
            <th>birthday</th>
            <th>address</th>
        </tr>
        </thead>
        <tbody>
        <?php while ($row = $result->fetch_assoc()): ?>
            <tr>
                <td><?= $row['sid'] ?></td>
                <td><?= $row['name'] ?></td>
                <td><?= $row['phone'] ?></td>
                <td><?= $row['email'] ?></td>
                <td><?= $row['birthday'] ?></td>
                <td><?= $row['address'] ?></td>

            </tr>
        <?php endwhile; ?>
        </tbody>
    </table>

    <table>
        <tr>
            <td>sid</td>
            <td>name</td>
            <td>phone</td>
            <td>email</td>
            <td>birthday</td>
            <td>address</td>
        </tr>
        <?php
        $result->data_seek(0);
        while ($row = $result->fetch_assoc()): ?>
            <tr>
                <td><?= $row['sid'] ?></td>
                <td><?= $row['name'] ?></td>
                <td><?= $row['phone'] ?></td>
                <td><?= $row['email'] ?></td>
                <td><?= $row['birthday'] ?></td>
                <td><?= $row['address'] ?></td>

            </tr>
        <?php endwhile; ?>
    </table>

</div>


</body>
</html>